package com.rybak.menutask.mapmenu;

public interface Printable {

    void print();
}
