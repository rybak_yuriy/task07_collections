package com.rybak.menutask.enummenu;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Menu[] menu = Menu.values();

        Menu option;
        do {
            System.out.println("Start");

            Menu.printMenu();

            int tmp = scanner.nextInt();
            while (tmp < 1 || tmp > 4){
                System.out.println("Wrong choice");
                System.out.println("Please select another option");
                tmp = scanner.nextInt();
            }


            option = menu[tmp-1];

            switch (option) {
                case FIRST:
                    System.out.println("You selected option FIRST");
                    break;
                case SECOND:
                    System.out.println("You selected option SECOND");
                    break;
                case THIRD:
                    System.out.println("You selected option THIRD");
                    break;
                case EXIT:
                    System.out.println("Good bye!");
            }
            System.out.println();
        } while (option != Menu.EXIT);

    }

}
