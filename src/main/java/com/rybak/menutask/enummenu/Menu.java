package com.rybak.menutask.enummenu;

import org.apache.logging.log4j.core.util.JsonUtils;

public enum Menu {

    FIRST("first option"),
    SECOND("second option"),
    THIRD("third option"),
    EXIT("exit app");


    private String option;

    Menu(String option) {
        this.option = option;
    }


    Menu() {

    }

    public static void printMenu() {
        Menu[] options = Menu.values();
        for (Menu option : options) {
            System.out.println(option.ordinal() + 1 + " | " + option.getOption());
        }
        System.out.println();
    }

    public String getOption() {
        return option;
    }

}
